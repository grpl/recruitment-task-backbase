import { OwmGroupInterface } from '@core/models/openWeatherMap/owmGroupInterface';

export const AllCitiesMock: OwmGroupInterface = {
  cnt: 5,
  list: [
    {
      coord: {
        lon: 21.01,
        lat: 52.23
      },
      sys: {
        country: 'PL',
        timezone: 7200,
        sunrise: 1597548096,
        sunset: 1597600743
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      main: {
        temp: 25.41,
        feels_like: 25.05,
        temp_min: 25,
        temp_max: 26.11,
        pressure: 1017,
        humidity: 37
      },
      visibility: 10000,
      wind: {
        speed: 0.45,
        deg: 23
      },
      clouds: {
        all: 0
      },
      dt: 1597572573,
      id: 756135,
      name: 'Warsaw'
    },
    {
      coord: {
        lon: -0.13,
        lat: 51.51
      },
      sys: {
        country: 'GB',
        timezone: 3600,
        sunrise: 1597553297,
        sunset: 1597605688
      },
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      main: {
        temp: 21.44,
        feels_like: 23.89,
        temp_min: 20.56,
        temp_max: 22.78,
        pressure: 1012,
        humidity: 84
      },
      visibility: 8088,
      wind: {
        speed: 0.87,
        deg: 45
      },
      clouds: {
        all: 76
      },
      dt: 1597572690,
      id: 2643743,
      name: 'London'
    },
    {
      coord: {
        lon: 2.35,
        lat: 48.85
      },
      sys: {
        country: 'FR',
        timezone: 7200,
        sunrise: 1597553117,
        sunset: 1597604678
      },
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      main: {
        temp: 19.84,
        feels_like: 20.75,
        temp_min: 18.33,
        temp_max: 21.11,
        pressure: 1009,
        humidity: 87
      },
      visibility: 10000,
      wind: {
        speed: 2.46,
        deg: 182
      },
      clouds: {
        all: 83
      },
      dt: 1597572774,
      id: 2988507,
      name: 'Paris'
    },
    {
      coord: {
        lon: 19.92,
        lat: 50.08
      },
      sys: {
        country: 'PL',
        timezone: 7200,
        sunrise: 1597548710,
        sunset: 1597600652
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      main: {
        temp: 24.44,
        feels_like: 25.46,
        temp_min: 24.44,
        temp_max: 24.44,
        pressure: 1014,
        humidity: 56
      },
      visibility: 10000,
      wind: {
        speed: 0.89,
        deg: 156
      },
      clouds: {
        all: 3
      },
      dt: 1597572813,
      id: 3094802,
      name: 'Krakow'
    },
    {
      coord: {
        lon: 2.16,
        lat: 41.39
      },
      sys: {
        country: 'ES',
        timezone: 7200,
        sunrise: 1597554109,
        sunset: 1597603776
      },
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      main: {
        temp: 27.2,
        feels_like: 30.97,
        temp_min: 26.11,
        temp_max: 27.78,
        pressure: 1011,
        humidity: 76
      },
      visibility: 10000,
      wind: {
        speed: 1.79,
        deg: 126
      },
      clouds: {
        all: 60
      },
      dt: 1597572826,
      id: 3128760,
      name: 'Barcelona'
    }
  ]
};
