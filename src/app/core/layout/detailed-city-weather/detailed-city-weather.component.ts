import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {filter} from 'rxjs/operators';
import { City } from '@core/models/app';
import {cleaningDataForCurrentCity, getWeatherDataForCityAction} from '@core/store/app.actions';
import {select, Store} from '@ngrx/store';
import { BackbaseWeatherState } from '@core/store/app.reducer';
import {getCityByIdSelector, getCurrentCitySelector} from '@core/store/app.selectors';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-detailed-city-weather',
  templateUrl: './detailed-city-weather.component.html',
  styleUrls: ['./detailed-city-weather.component.scss']
})
export class DetailedCityWeatherComponent implements OnInit, OnDestroy {
  cityData$: Observable<City>;
  currentCity$: Observable<BackbaseWeatherState['currentCity']>;

  constructor(
    private route: ActivatedRoute,
    private store: Store<BackbaseWeatherState>,
  ) { }

  ngOnInit(): void {
    const cityId: number = Number(this.route.snapshot.paramMap.get('id'));

    this.loadWeatherDataForCity(cityId);

    this.currentCity$ = this.store.pipe(
      select(getCurrentCitySelector),
      filter((currentCity: BackbaseWeatherState['currentCity'])  => currentCity !== null)
    );

    this.cityData$ = this.store.pipe(
      select( state => getCityByIdSelector(state, cityId)),
      filter((cityData: City)  => cityData !== null)
    );
  }

  ngOnDestroy(): void {
    this.store.dispatch(cleaningDataForCurrentCity());
  }

  private loadWeatherDataForCity(cityId: City['id']): void {
    this.store.dispatch(getWeatherDataForCityAction({ data: { cityId } }));
  }
}
