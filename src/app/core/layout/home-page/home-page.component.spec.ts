import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePageComponent } from './home-page.component';
import { provideMockStore } from '@ngrx/store/testing';
import { getCityWeatherDataListSelector } from '@core/store/app.selectors';
import { StoreModule, StoreRootModule } from '@ngrx/store';

describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreRootModule,
        StoreModule.forRoot({})
      ],
      declarations: [ HomePageComponent ],
      providers: [
        provideMockStore({
          selectors: [
            { selector: getCityWeatherDataListSelector, value: '' },
          ],
        })
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  afterEach(() => { fixture.destroy(); });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
