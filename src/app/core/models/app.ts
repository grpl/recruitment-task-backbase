export interface City {
  id: number;
  name: string;
  lat: number;
  lon: number;
}

export const InitListOfCities = Object.freeze({
  3128760: {
    id: 3128760,
    name: 'Barcelona',
    lat: 41.38879,
    lon: 2.15899
  },
  3094802: {
    id: 3094802,
    name: 'Krakow',
    lat: 50.049683,
    lon: 19.944544
  },
  2643743: {
    id: 2643743,
    name: 'London',
    lat: 51.50853,
    lon: -0.12574
  },
  756135: {
    id: 756135,
    name: 'Warsaw',
    lat: 52.237049,
    lon: 21.017532
  },
  2988507: {
    id: 2988507,
    name: 'Paris',
    lat: 48.864716,
    lon: 2.349014
  }
});
