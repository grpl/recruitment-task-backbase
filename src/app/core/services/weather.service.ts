import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { City } from '../models/app';
import { OwmOneCallInterface } from '@core/models/openWeatherMap/owmOneCallInterface';
import { OwmGroupInterface } from '@core/models/openWeatherMap/owmGroupInterface';
import { map } from 'rxjs/operators';
import { BackbaseWeatherState } from '@core/store/app.reducer';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private apiKey = environment.apiKeyForOpenWeatherMap;
  private prefixApiUrl = 'http://api.openweathermap.org/data/';
  private apiVersion = '2.5';

  private oneCallApiUrl = `${this.prefixApiUrl}${this.apiVersion}/onecall?units=metric&exclude=daily,minutely&APPID=${this.apiKey}`;
  private groupApiUrl = `${this.prefixApiUrl}${this.apiVersion}/group?units=metric&APPID=${this.apiKey}`;

  constructor(
    protected httpClient: HttpClient
  ) { }

  getWeatherDataForCity(city: City): Observable<OwmOneCallInterface> {
    return this.httpClient.get<OwmOneCallInterface>(`${this.oneCallApiUrl}&lat=${city.lat}&lon=${city.lon}`);
  }

  getWeatherDataForAllCities(cities: BackbaseWeatherState['initListOfCities']): Observable<OwmGroupInterface['list']> {
    const cityIdsToString = Object.keys(cities).join(',');

    return this.httpClient
      .get<OwmGroupInterface>(`${this.groupApiUrl}&id=${cityIdsToString}`)
      .pipe(
        map( (group: OwmGroupInterface ) => group.list )
      );
  }
}
