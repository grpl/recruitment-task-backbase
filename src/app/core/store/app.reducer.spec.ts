import { backbaseWeatherReducer, initialBackbaseWeatherState } from '@core/store/app.reducer';
import {
  cleaningDataForCurrentCity,
  getWeatherDataForAllCitiesSuccessAction,
  getWeatherDataForCitySuccessAction
} from '@core/store/app.actions';
import { OneCallWarsawMock } from '../../api-mocks/open-weather-map/one-call/one-call-warsaw-mock';
import { AllCitiesMock } from '../../api-mocks/open-weather-map/group/all-cities';

describe('backbaseWeatherReducer', () => {
  describe('successfully get data for the city', () => {
    it('should change store', () => {
      const action = getWeatherDataForCitySuccessAction({data: { currentCity: OneCallWarsawMock }});
      const newState = backbaseWeatherReducer(initialBackbaseWeatherState, action);
      const expectedState = {...initialBackbaseWeatherState, currentCity: OneCallWarsawMock};
      expect(newState).toEqual(expectedState);
    });
  });

  describe('successfully get data for group of cities', () => {
    it('should change store', () => {
      const action = getWeatherDataForAllCitiesSuccessAction({data: { cityWeatherDataList: AllCitiesMock.list }});
      const newState = backbaseWeatherReducer(initialBackbaseWeatherState, action);
      const expectedState = {...initialBackbaseWeatherState, cityWeatherDataList: AllCitiesMock.list};
      expect(newState).toEqual(expectedState);
    });
  });

  describe('cleaning data for the city', () => {
    it('should change store', () => {
      const action = cleaningDataForCurrentCity();
      const newState = backbaseWeatherReducer({...initialBackbaseWeatherState, currentCity: OneCallWarsawMock}, action);
      const expectedState = {...initialBackbaseWeatherState};
      expect(newState).toEqual(expectedState);
    });
  });
});
