import {
  getCityByIdSelector,
  getCityWeatherDataListSelector,
  getCurrentCitySelector,
  listOfCitiesSelector
} from '@core/store/app.selectors';
import { backbaseWeatherFeatureKey, initialBackbaseWeatherState } from '@core/store/app.reducer';
import { City } from '@core/models/app';
import { listOfCitiesMock } from '../../api-mocks/list-of-cities';
import { OneCallWarsawMock } from '../../api-mocks/open-weather-map/one-call/one-call-warsaw-mock';
import { AllCitiesMock } from '../../api-mocks/open-weather-map/group/all-cities';

describe('Backbase weather selectors', () => {
  const mockCityId: City['id'] = Number(Object.keys(listOfCitiesMock)[0]);

  it('should select the list of cities', () => {
    const result = listOfCitiesSelector(
      {
        [backbaseWeatherFeatureKey]: {...initialBackbaseWeatherState, initListOfCities: listOfCitiesMock}
      }
    );
    expect(result).toEqual(listOfCitiesMock);
  });

  it('should select the city by id', () => {
    const result = getCityByIdSelector(
      {
        [backbaseWeatherFeatureKey]: {...initialBackbaseWeatherState, initListOfCities: listOfCitiesMock}
      },
      mockCityId
    );
    expect(result).toEqual(listOfCitiesMock[mockCityId]);
  });

  it('should select the current city', () => {
    const result = getCurrentCitySelector(
      {
        [backbaseWeatherFeatureKey]: {...initialBackbaseWeatherState, currentCity: OneCallWarsawMock}
      }
    );
    expect(result).toEqual(OneCallWarsawMock);
  });

  it('should select weather data for the list of cities', () => {
    const result = getCityWeatherDataListSelector(
      {
        [backbaseWeatherFeatureKey]: {...initialBackbaseWeatherState, cityWeatherDataList: AllCitiesMock.list}
      }
    );
    expect(result).toEqual(AllCitiesMock.list);
  });
});
