import { props, createAction } from '@ngrx/store';
import { City } from '@core/models/app';
import {OwmOneCallInterface} from '@core/models/openWeatherMap/owmOneCallInterface';
import {OwmGroupInterface} from '@core/models/openWeatherMap/owmGroupInterface';

// [ACTIONS] Get weather data for all cities
export const getWeatherDataForAllCitiesAction = createAction(
  '[Weather Cities] get weather data for the all cities'
);

export const getWeatherDataForAllCitiesSuccessAction = createAction(
  '[Weather Cities] successfully loading weather data for the cities',
  props<{ data: { cityWeatherDataList: OwmGroupInterface['list'] } }>()
);

export const getWeatherDataForAllCitiesFailureAction = createAction(
  '[Weather Cities] failure loading weather data for the cities'
);
// [ACTIONS] Get weather data for all cities - END


// [ACTIONS] Get weather data for city
export const getWeatherDataForCityAction = createAction(
  '[Weather City] starting load weather data for the city',
  props<{ data: { cityId: City['id'] } }>()
);

export const getWeatherDataForCitySuccessAction = createAction(
  '[Weather City] successfully loading weather data for the city',
  props<{ data: { currentCity: OwmOneCallInterface } }>()
);

export const getWeatherDataForCityFailureAction = createAction(
  '[Weather City] failure loading weather data for the city'
);

export const cleaningDataForCurrentCity = createAction(
  '[Weather City Cleaning] cleaning data for the city'
);
// [ACTIONS] Get weather data for city - END
