import {
  cleaningDataForCurrentCity,
  getWeatherDataForAllCitiesAction,
  getWeatherDataForAllCitiesFailureAction,
  getWeatherDataForAllCitiesSuccessAction,
  getWeatherDataForCityAction,
  getWeatherDataForCityFailureAction,
  getWeatherDataForCitySuccessAction
} from './app.actions';
import { OwmOneCallInterface } from '../models/openWeatherMap/owmOneCallInterface';
import { OwmGroupInterface } from '../models/openWeatherMap/owmGroupInterface';

describe('Weather data', () => {
  describe('for city', () => {
    it('should return an action', () => {
      expect(getWeatherDataForCityAction({data: { cityId: 5 }}).type)
        .toBe('[Weather City] starting load weather data for the city');
    });

    it('should return an action', () => {
      expect(getWeatherDataForCitySuccessAction({ data: { currentCity: {} as OwmOneCallInterface } }).type)
        .toBe('[Weather City] successfully loading weather data for the city');
    });

    it('should return an action', () => {
      expect(getWeatherDataForCityFailureAction().type)
        .toBe('[Weather City] failure loading weather data for the city');
    });

    it('should return an action', () => {
      expect(cleaningDataForCurrentCity().type)
        .toBe('[Weather City Cleaning] cleaning data for the city');
    });
  });

  describe('for all cities', () => {
    it('should return an action', () => {
      expect(getWeatherDataForAllCitiesAction().type)
        .toBe('[Weather Cities] get weather data for the all cities');
    });

    it('should return an action', () => {
      expect(getWeatherDataForAllCitiesSuccessAction({ data: { cityWeatherDataList: [] as OwmGroupInterface['list'] } }).type)
        .toBe('[Weather Cities] successfully loading weather data for the cities');
    });

    it('should return an action', () => {
      expect(getWeatherDataForAllCitiesFailureAction().type)
        .toBe('[Weather Cities] failure loading weather data for the cities');
    });
  });
});
