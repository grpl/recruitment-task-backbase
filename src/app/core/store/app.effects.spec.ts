import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { ReplaySubject, of, throwError } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AppEffects } from './app.effects';
import { WeatherService } from '../services/weather.service';
import { getCityByIdSelector, listOfCitiesSelector } from './app.selectors';
import { listOfCitiesMock } from '../../api-mocks/list-of-cities';
import {
  getWeatherDataForAllCitiesAction,
  getWeatherDataForAllCitiesFailureAction,
  getWeatherDataForAllCitiesSuccessAction,
  getWeatherDataForCityAction,
  getWeatherDataForCityFailureAction,
  getWeatherDataForCitySuccessAction
} from './app.actions';
import isEqual from 'lodash.isequal';
import { AllCitiesMock } from '../../api-mocks/open-weather-map/group/all-cities';
import { HttpErrorResponse } from '@angular/common/http';
import { City } from '@core/models/app';
import { OneCallWarsawMock } from '../../api-mocks/open-weather-map/one-call/one-call-warsaw-mock';

describe('TendersEffects', () => {
  let actions$: ReplaySubject<any>;
  let effects: AppEffects;
  let weatherService: WeatherService;
  let store: MockStore;
  const mockBackendError = { error: { error: { error: 'error' } } } as HttpErrorResponse;
  const mockCityId: City['id'] = Number(Object.keys(listOfCitiesMock)[0]);
  const mockCity: City = Object.values(listOfCitiesMock)[0];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        AppEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          selectors: [
              {
                selector: listOfCitiesSelector,
                value: listOfCitiesMock
              },
              {
                selector: getCityByIdSelector,
                value: mockCity
              }
            ]
          }
        ),
        {
          provide: WeatherService,
          useValue: jasmine.createSpyObj('weatherService', ['getWeatherDataForCity', 'getWeatherDataForAllCities']),
        },
      ]
    });
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    effects = TestBed.inject(AppEffects);
    weatherService = TestBed.inject(WeatherService);
    (weatherService.getWeatherDataForCity as jasmine.Spy)
      .and
      .returnValue(of(OneCallWarsawMock));
    (weatherService.getWeatherDataForAllCities as jasmine.Spy)
      .and
      .returnValue(of(AllCitiesMock.list));
  });

  describe('getWeatherDataForAllCitiesEffect$', () => {
    beforeEach(() => {
      actions$ = new ReplaySubject(1);
      actions$.next(getWeatherDataForAllCitiesAction());
    });

    it('should be created', async () => {
      expect(effects).toBeTruthy();
    });

    it('should return getWeatherDataForAllCitiesSuccessAction on success', () => {
      effects.getWeatherDataForAllCitiesEffect$
        .subscribe( (resultAction) => {
          expect(isEqual(resultAction, getWeatherDataForAllCitiesSuccessAction({data: {cityWeatherDataList: AllCitiesMock.list}})))
            .toBeTruthy();

          expect(weatherService.getWeatherDataForAllCities).toHaveBeenCalledWith(listOfCitiesMock);
        });
    });

    it('should return getWeatherDataForAllCitiesSuccessAction on failure', () => {
      (weatherService.getWeatherDataForAllCities as jasmine.Spy)
        .and
        .returnValue(throwError(mockBackendError));

      effects.getWeatherDataForAllCitiesEffect$
        .subscribe( ({type}) => {
          expect(getWeatherDataForAllCitiesFailureAction().type).toEqual(type);
        });
    });
  });

  describe('getWeatherDataForCityEffect$', () => {

    beforeEach(() => {
      actions$ = new ReplaySubject(1);
      actions$.next(getWeatherDataForCityAction({ data: { cityId: mockCityId } }));
    });

    it('should be created', async () => {
      expect(effects).toBeTruthy();
    });

    it('should return getWeatherDataForCitySuccessAction on success', () => {
      effects.getWeatherDataForCityEffect$
        .subscribe( (resultAction) => {
          expect(isEqual(resultAction, getWeatherDataForCitySuccessAction({data: { currentCity: OneCallWarsawMock }})))
            .toBeTruthy();

          expect(weatherService.getWeatherDataForCity).toHaveBeenCalledWith(mockCity);
        });
    });

    it('should return getWeatherDataForCityFailureAction on failure', () => {
      (weatherService.getWeatherDataForCity as jasmine.Spy)
        .and
        .returnValue(throwError(mockBackendError));

      effects.getWeatherDataForCityEffect$
        .subscribe( ({type}) => {
          expect(getWeatherDataForCityFailureAction().type).toEqual(type);
        });
    });
  });
});
