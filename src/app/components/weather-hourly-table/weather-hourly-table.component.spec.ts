import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherHourlyTableComponent } from './weather-hourly-table.component';
import { provideMockStore } from '@ngrx/store/testing';
import { getCurrentCitySelector } from '@core/store/app.selectors';

describe('WeatherHourlyTableComponent', () => {
  let component: WeatherHourlyTableComponent;
  let fixture: ComponentFixture<WeatherHourlyTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherHourlyTableComponent ],
      providers: [
        provideMockStore({
          selectors: [
            { selector: getCurrentCitySelector, value: '' }
          ]
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherHourlyTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => { fixture.destroy(); });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
