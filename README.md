# Backbase

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

## UI/UX

W projekcie użyłem bibliotekę Bootstrap 4. Wykorzystałem z niej jedynie niezbędne moduły. 

Dołączam je tutaj: `Backbase\src\styles.scss`

Zastosowałem zbiór regół CSS do "resetowania" styli w przeglądarkach.
`@import "~bootstrap/scss/reboot";`

Użyłem zastępczego zdjęcia z serwisu: `http://placeimg.com/`
I dodałem do URL unikalne ID aby każde zdjęcie było inne i aby nie było brane z cache przeglądarki.

Przykład: `http://placeimg.com/188/140/arch?n={{ city.id }}`

Dodałem przekierowanie na podstronę z informacją o błędnym adresie URL.
`http://localhost:4200/test`

Dodałem spinner dla ładowania danych.

Użytkownik może przesyłać stronę z szczegółami pogody dla miasta przez komunikator.
Id miasta w URL. 

Przykład: `http://localhost:4200/detailed-city-weather/3094802`

## Architekrura

Przepływ danych oparłem na NgRx. Maksymalnie uprościłem jego użycie w komponentach.
Całą resztę operacji przeniosłem do effektów, selektów i serwisu.

Przykład: `Backbase\src\app\core\store\app.effects.ts`


Odwzorowałem interfejsy z API Open Weather

Przykład: `Backbase\src\app\core\models\openWeatherMap\owmGroupInterface.ts`


Stworzyłem model danych dla listy dat tak aby unikalnym kluczem był ID miasta.
Pozwoliło to na skróceniu czasu wyszukania danych miasta na podstronie do O(1).

Przykład: `  3128760: {
               id: 3128760,
               name: 'Barcelona',
               lat: 41.38879,
               lon: 2.15899
             }`
  
             
Dla testów stworzyłem katalog z mokowymi danymi.

Przykład: `Backbase\src\app\api-mocks`


Klucz API Open Weather dodałem do plików environment, aby umożliwić jego szybką zmienę i dynamiczną zamianę.

Przykład: `apiKeyForOpenWeatherMap: '78bfdc9a1dfd820ee0fc44bc30fb1c43'`


Czyszczenie danych pogodowych podczas przełączania pomięczy stronami.

Przykład: `  ngOnDestroy(): void {
               this.store.dispatch(cleaningDataForCurrentCity());
             }`
            
## Usprawnienia na przyszłość
Dodanie modułu translacji.

Dodanie wykresu lub opracowanie innego komponentu do prezentacji szczegułów pogody dla miasta.

Dodanie cache dla pobierania danych.
Cache dla miasta: sprawdzanie aktualnej godziny. Cache dla listy miast: sprawdzanie po dniu.

Mikroanimacje.

Breadcrumbs.

Dodanie SSR i PWA.

Docker. 

Testy E2E.
